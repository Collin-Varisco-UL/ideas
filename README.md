# Database Design

## Query
### (1) Client Names
### (2) What CGI Is Selling
1. Create a database table of the clients of interest. 

| Table Name  | Schema |
|---|---|
| ClientNames   | ( ClientName [PK] )  |  

2. Create a database table of services that fall under one of the following categories.
    - Systems integration
    - Application services
    - Managed IT outsourcing services
    - Infrastructure services
    - Business process services
    - Business consulting

This table will require the name of the service, the category, and an acronym if it is commonly used industry in the industry to refer to the service. (Services schema)

A second table will contain alternative names that may be used for that service (See ServiceAliases schema)
    
| Table Name  | Schema |
|---|---|
| Services | ( ServiceID [PK], NameOfService, Category, Acronym) |
| ServiceAliases | ( ServiceID [FK], Alias)

For each client, the backend software will look through different sources of information to see which
clients need any of the services that we offer. When opportunities are found, the companies and the service
they require will be added to a database table. A date will be recorded so that entries can be dropped automatically
after a certain amount of time has passed.

| Table Name  | Schema |
|---|---|
| ClientOpportunities | ( ClientName [FK], ServiceID [FK], ServiceDate ) |



